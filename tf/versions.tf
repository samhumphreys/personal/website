terraform {
  required_version = "1.7.4"

  backend "gcs" {
    bucket = "shumphreys-tf-state"
    prefix = "terraform/state"
  }

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 5.17.0"
    }
  }
}
