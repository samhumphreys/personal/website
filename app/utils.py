import random
import string


def random_string() -> str:
    """Generate a random string containing ascii letters & numbers"""
    return "".join(random.sample(list(string.ascii_letters + string.digits), 20))
