import flask

import app.utils

svc = flask.Flask(__name__)
svc.secret_key = app.utils.random_string()

LOG = svc.logger


@svc.route("/healthcheck")
def healthcheck():
    """Healthcheck route"""
    return "OK", 200


@svc.route("/", methods=["GET"])
def home():
    """Landing page route"""
    return flask.render_template("home.html")


@svc.route("/experience", methods=["GET"])
def experience():
    """Experience page route"""
    return flask.render_template("experience.html")


@svc.route("/ip")
def ip():
    """Return client IP address"""
    return flask.jsonify({"ip": flask.request.remote_addr}), 200


if __name__ == "__main__":
    svc.run(host="0.0.0.0")
