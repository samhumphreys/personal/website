#!/bin/bash
gcp_authenticate() {
  # Argument is SA JSON credentials (base64 encoded)
  echo $1 | base64 -d > credentials.json
  gcloud auth activate-service-account --key-file=credentials.json
  gcloud config set project $GCP_PROJECT
}
