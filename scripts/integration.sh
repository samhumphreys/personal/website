#!/bin/bash
validate_endpoint () {
  url=$1
  path=$2
  exit_code=$3
  url_path="${url}${path}"

  echo "Hitting URL ${url_path}"
  response_code=$(curl --silent --head "${url_path}" | awk '/^HTTP/{print $2}')

  if [[ ! "${response_code}" -eq "${exit_code}" ]]; then
    echo "Requested path (${path}) returned exit code ${exit_code}, expected ${exit_code}"
    exit 1
  fi
}
