import pytest

from app import svc


@pytest.fixture()
def app():
    app = svc
    yield app


@pytest.fixture()
def client(app):
    return app.test_client()


def test_root(client):
    response = client.get("/")
    assert response.status_code == 200
    assert b"<h1>Sam Humphreys</h1>" in response.data


def test_healthcheck(client):
    response = client.get("/healthcheck")
    assert response.status_code == 200
    assert "OK" == response.data.decode()


def test_experience(client):
    response = client.get("/experience")
    assert response.status_code == 200
    assert b'<h1><a class="return-arrow" href="/"><</a>Experience</h1>' in response.data


def test_ip(client):
    response = client.get("/ip")
    assert response.status_code == 200
    assert "ip" in response.json.keys()
