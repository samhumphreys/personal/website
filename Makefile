app_yaml := app.yaml

test-deploy:
	gcloud app deploy --no-promote --appyaml=$(app_yaml)
